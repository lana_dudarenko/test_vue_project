export default {
  addProduct ({ commit }, { name, colors }) {
    commit('addProduct', { name, colors })
  },

  deleteProduct ({ commit }, product) {
    commit('deleteProduct', product)
  },
}
