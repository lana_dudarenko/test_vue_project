export const STORAGE_KEY = 'productsList-vuejs'

export const mutations = {
  addProduct (state, product) {
    state.products.push(product)
  },

  deleteProduct (state, product) {
    state.products.splice(state.products.indexOf(product), 1)
  },
}
