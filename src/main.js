import Vue from 'vue'
import VeeValidate from 'vee-validate'
import VueRouter from 'vue-router'
//Store
import store from './store'
//Components
import App from './App.vue'
import ProductList from './components/ProductList.vue'
import ProductForm from './components/ProductForm.vue'
import PageNotFound from './components/PageNotFound.vue'

Vue.use(VeeValidate);
Vue.use(VueRouter);

Vue.config.productionTip = false;

const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: [
    { path: '/', redirect: '/product-form' },
    { path: '/product-list', component: ProductList },
    { path: '/product-form', component: ProductForm },
    { path: '*', component: PageNotFound },
  ]
})

new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app')

